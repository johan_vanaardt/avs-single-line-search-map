angular
  .module('SingleLineSearchApp', ['ngMaterial', 'leaflet-directive'])
  .controller('addressCtrl', addressCtrl)

function addressCtrl($timeout, $q, $log, $http, $scope, leafletData) {
  var ctrl = this;

  ctrl.querySearch = querySearch;
  ctrl.selectedItemChange = selectedItemChange;
  ctrl.candidates;
  ctrl.info = '';
  ctrl.scope = $scope;

  var host = 'http://localhost:8000';

  loadMap(0, 0, 1, ctrl.scope);

  function querySearch(query) {
    ctrl.candidates = [];   
    var results = $http.get(host + '/getByName/' + query)
      .then(function (searchCandidates) {
        var pointAddresses = searchCandidates.data.pointAddresses;
        var farms = searchCandidates.data.farms;
        var pointOfInterests = searchCandidates.data.pointOfInterests;
        var adminAreas = searchCandidates.data.adminAreas;
        var roads = searchCandidates.data.roads; 
              

        if(pointAddresses.length == 0 && farms.length == 0 && pointOfInterests.length == 0 && adminAreas.length == 0 && roads.length == 0){          
          ctrl.candidates = [];
        }
        else {      
          if(pointAddresses.length != 0){
            for (var i = 0; i < pointAddresses.length; i++) {
              var address = pointAddresses[i].address;
              ctrl.candidates.push({
                display: address.roadNumber + ' ' +
                  address.roadName + ' ' +
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: "point address"
              })          
            }
          }    
          if(farms.length != 0){
            for (var i = 0; i < farms.length; i++) {
              var address = farms[i].address;
              ctrl.candidates.push({
                display: address.name + ' ' +                  
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: "farm"
              })              
            }
          }   
          if(pointOfInterests.length != 0){
            for (var i = 0; i < pointOfInterests.length;i++) {
              var address = pointOfInterests[i].address;
              ctrl.candidates.push({
                display: address.name + ' ' + address.suburbName + ' ' +
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: "point of interest"
              }   )           
            }
          }   
          if(roads.length != 0){
            for (var i = 0; i < roads.length; i++) {
              var address = roads[i].address;
              ctrl.candidates.push({
                display: address.name + ' ' +                  
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: "road"
              } )             
            }
          }      
          if(adminAreas.length != 0){
            for (var i = 0; i < adminAreas.length; i++) {
              var address = adminAreas[i].address;
              ctrl.candidates.push({
                display: address.suburbName + ' ' +                  
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: address.areaType.toLowerCase(),
                areaType: address.areaType
              })              
            }
          }                       
        }
        return ctrl.candidates;
      }); 
    return results;
  }

  function selectedItemChange(item) {
    var data = '';    
    ctrl.info = '';
    if (item != undefined) {    
      if(item.type == 'point address'){
        data = 'addresses';
      }else
      if(item.type == 'farm'){
        data = 'farms';
      }else
      if(item.type == 'point of interest'){
        data = 'point-of-interests';
      }else
      if(item.type == 'road'){
        data = 'roads';
      }else
      if (item.type == 'suburb' || item.type == 'town') {
        if(item.areaType == 'SUBURB'){
          data = 'suburbs';
        } else if(item.areaType == 'TOWN'){
          data = 'towns';
        }              
      }
    var results = $http.get(host + '/getById/' +data +'/'+ item.value)
      .then(function (info) {
        ctrl.info = info.data.avsInfo;

        loadMap(ctrl.info.latitude, ctrl.info.longitude, 16, ctrl.scope);
        loadMarker(ctrl.info.latitude, ctrl.info.longitude, ctrl.scope);
        return ctrl.info;
      });
    }
    return results;
  }

  leafletData.getMap('property-overview-image').then(function (map) {   
    map.setZoom(1);
    setTimeout(function () { map.invalidateSize() }, 0);
    map._resetView(map.getCenter(), map.getZoom(), true);
  });
}

function loadMap(lat, long, zoom, $scope) {
  angular.extend($scope, {
    center: {
      lat: parseFloat(lat, 10),
      lng: parseFloat(long, 10),
      zoom: parseFloat(zoom, 10)
    },
    tiles: {
      url: "http://api.tiles.mapbox.com/v4/mapbox.streets-satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZ3JpcHBsYXRmb3JtdG9vbCIsImEiOiJjaXh1OTlxYnowMDI0MnhxcWl3ZTNlc3Q4In0.L0M749vJVUYO5s5vkY49YQ"
    }
  });
}

function loadMarker(lat, long, $scope) {
  var mapIcon = {
    iconUrl: './assets/img/map-marker.png',
    iconSize: [30, 50],
    iconAnchor: [15, 25]
  }
  angular.extend($scope, {
    markers: {
      osloMarker: {
        icon: mapIcon,
        lat: parseFloat(lat, 10),
        lng: parseFloat(long, 10),
        focus: true,
        draggable: false
      }
    },
    defaults: {
      scrollWheelZoom: false
    }
  });
}