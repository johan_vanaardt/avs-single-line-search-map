var express = require('express');
var requestJSON = require('request-json');
require('dotenv-safe').config();

var app = express();

app.use(express.static('./'));
app.use(express.static('public'));

app.get('/', function(req, res) {
    // We must end the request when we are done handling it
    res.end();
  });

app.get('/getByName/:data', function (req, res) {
    var data = req.params.data
    var headers = {};    
    var endpoint = 'api/searches/single-line';
    endpoint += '?' + 'searchString=' + encodeURI(data);    

    var client = requestJSON.createClient(process.env.AVS4_BASE_URL);
    client.headers['accept'] = 'application/json';
    client.headers['client-key'] = process.env.AVS4_CLIENT_KEY;
    client.get(endpoint, function (error, response, body) {
        if (!error && response.statusCode == 200) { 
            var responseData = body.searchCandidates;
            res.send(responseData);
            return responseData;            
        }
        console.log(error);
    })  
})

app.get('/getById/:type/:data', function (req, res) {
    var data = req.params.data;
    var type = req.params.type;
    var headers = {};    
    var endpoint = 'api/';        
    endpoint += type +'/'+ data;
    var client = requestJSON.createClient(process.env.AVS4_BASE_URL);
    client.headers['accept'] = 'application/json';
    client.headers['client-key'] = process.env.AVS4_CLIENT_KEY;
    client.get(endpoint, function (error, response, body) {
        if (!error && response.statusCode == 200) { 
            var responseData = body;
            res.send(responseData);
            return responseData;            
        }
        console.log(error);
    })  
})

app.listen(process.env.PORT,() =>{
    console.log(`server started at :${process.env.PORT}`);
});